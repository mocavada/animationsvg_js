$(function(){
    console.log( 'jQuery started.' );
    
    function svgLoadComplete(){
        $( '#stroke-text' ).css({ display: 'none' });
    
        $( '#button' ).on( 'click', function(){
            console.log( 'button was clicked' );
            $( '#stroke-text' ).css({ display: '' });

            var delay = 0;
            $( '#stroke-text *' ).each(function(){
                var $letter = $(this);

                var length = Math.round( $letter.get(0).getTotalLength() );

                $letter.css({
                    'stroke-dasharray' : length.toString(),
                    'stroke-dashoffset' : length.toString()
                });

                setTimeout(function(){
                    $letter.attr( 'class', 'animate' );
                }, delay );
                delay += 1000;
            });
        });
        
        // $( '#star' ).css({ transform: 'translate(100px,0)' });
    
        $( '#star' ).velocity({ 
            translateX : 100 },{
            duration : 1000
        }).velocity({ 
            translateY : 200 },{
            duration : 2000
        }).velocity({ 
            rotateZ : -180 },{
            duration : 1000
        })
    }
	
});

