$(function(){
    console.log( 'jQuery started.' );
    
    // Use AJAX (get method) to load the large SVG file
    // when load is finished, run the svgLoadComplete function
    $.get( 'images/example.svg', svgLoadComplete, 'text' );
    
    // a place to store the text of the SVG file
    var svg = null;
    
    /**
     * Accepts a loaded SVG file, then hides the
     * spinner that is on the page.
     * @param {string} svgData - The text of the loaded SVG document
     * @callback
     */
    function svgLoadComplete( svgData ){
        console.log( 'svg loaded.' );
        $( '#spinner' ).velocity({ opacity: 0 }, 
                                 { duration: 500, 
                                  complete: startSVGAnimation } );
        svg = svgData;                        
    }
         
    
    /**
     * Removes the spinner and embeds the SVG document.
     * @callback
     */
    function startSVGAnimation(){                             
           
        console.log( 'svg animation starting.' );
        
        // delete the spinner from the HTML
        $( '#spinner' ).remove();
        
        // embed the svg document inside the main tag
        $( 'main' ).html( svg );
        
        // TODO: get this working (should fade in the svg)
        $( '#example' )
            .css({ opacity: 0 })
            .velocity({ opacity: 1 },{ duration: 2000 });
        
        // hide the line writing text
        $( '#stroke-text' ).css({ display: 'none' });
    
        // click handler for button in the SVG file
        $( '#button' ).on( 'click', function(){
            console.log( 'button was clicked' );
            
            // make the line writing text visible
            $( '#stroke-text' ).css({ display: '' });

            var delay = 0;
            $( '#stroke-text *' ).each(function(){
                // apply the line animation to each shape
                // within the #stroke-text group
                
                // get the current letter/shape
                var $letter = $(this);

                // calculate the length of the line in the letter/shape
                var length = Math.round( $letter.get(0).getTotalLength() );

                // apply the lengths to the css
                $letter.css({
                    'stroke-dasharray' : length.toString(),
                    'stroke-dashoffset' : length.toString()
                });

                // apply the animation class, using
                // a delay to stagger the animations
                setTimeout(function(){
                    $letter.attr( 'class', 'animate' );
                }, delay );
                delay += 1000;
            });
        });
        
        // $( '#star' ).css({ transform: 'translate(100px,0)' });
    
        // move the star around
        $( '#star' ).velocity({ 
            translateX : 100 },{
            duration : 1000
        }).velocity({ 
            translateY : 200 },{
            duration : 2000
        }).velocity({ 
            rotateZ : -180 },{
            duration : 1000
        })
    }
});








